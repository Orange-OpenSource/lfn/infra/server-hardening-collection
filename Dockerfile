FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core:2.11
LABEL maintainer="Sylvain Desbureaux <sylvain.desbureaux@orange.com>"

ARG VCS_REF
ARG BUILD_DATE

ENV APP /opt/hardening/
ENV COLLECTION_PATH ~/.ansible/collections

WORKDIR $APP

COPY . $APP/

RUN apk add --no-cache git-lfs=~3.0 && \
    mkdir -p "$COLLECTION_PATH" && \
    output="$(date +%s)" && \
    ansible-galaxy collection build --output-path "/tmp/$output" && \
    tarball="$(ls -1 "/tmp/$output")" && \
    ansible-galaxy collection install "/tmp/$output/$tarball" \
      -p "$COLLECTION_PATH" && \
    rm -fr "/tmp/$output"
