#!/bin/bash

# SPDX-license-identifier: Apache-2.0
##############################################################################
# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
##############################################################################

set -o errexit
set -o nounset
set -o pipefail

labels=$*

RUN_SCRIPT=${0}
RUN_ROOT=$(dirname $(readlink -f ${RUN_SCRIPT}))
export RUN_ROOT=$RUN_ROOT
source ${RUN_ROOT}/scripts/rc.sh

# register our handler
trap submit_bug_report ERR

#-------------------------------------------------------------------------------
# If no labels are set with args, run all
#-------------------------------------------------------------------------------
if [[ $labels = "" ]]; then
  labels="run test"
fi

if [[ $labels = *"run"* ]]; then
  #-------------------------------------------------------------------------------
  # Run Hardening
  #-------------------------------------------------------------------------------
  step_banner "Run hardening role"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/run.yml --vault-id ${RUN_ROOT}/.vault

  step_banner "Hardening done"
fi

#-------------------------------------------------------------------------------
# test installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test"* ]]; then
  step_banner "test that hardening is OK"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Hardening is OK"
fi
