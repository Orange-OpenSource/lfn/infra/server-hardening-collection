# A collection to harden a host with CentOS 8 or Ubuntu 20.04

This repo contains the `orange.server_hardening` Ansible Collection. The aim
of this collection is to harden a system according to CIS Benchmarks.
For CentOS 8, not all the rules are checked, this implementation is not
exhaustive regarding the vulnerabilities described below.
However, the main vulnerabilities that are often on a FAIL state on a fresh
CentOS 8 installation are checked.
For Ubuntu 20.04, the role ansible-lockdown/UBUNTU20-CIS is used,
see <https://github.com/ansible-lockdown/UBUNTU20-CIS/>

## Requirement

For the CentOS part.
One mandatory requirement is to have an environnment variable BOOT_PASSWD set
to a value representing the bootloader password. This is a requirement for
1.5.2 rule.

For the Ubuntu part.
You can have a file in your inventory for overriding the variables defined
in the Ansible lockdown Ubuntu 20.04 hardening role.
For further information, please take a look at the doc `docs/roles/lockdown.md`.

## Using this collection

Before using this collection, you need to install it with the `ansible-galaxy` CLI:

```shell
ansible-galaxy collection install https://gitlab.com/Orange-OpenSource/lfn/infra/server-hardening-collection
```

use `,branch_or_tag` if you want to use a specific branch version:

```shell
ansible-galaxy collection install https://gitlab.com/Orange-OpenSource/lfn/infra/server-hardening-collection.git,v1
```

You can also include it in a `requirements.yml` file and install it via
`ansible-galaxy collection install -r requirements.yml` using the format:

```yaml
collections:
  - name: git+https://gitlab.com/Orange-OpenSource/lfn/infra/server-hardening-collection.git
    type: git
    version: master
```

See [Ansible Using collections](
  https://docs.ansible.com/ansible/latest/user_guide/collections_using.html) for
more details.

## Run the OS hardening

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/run.yml
```

## CIS Benchmarks for CentOS 8 and Ubuntu

```yaml
1 Initial setup
  1.1 Filesystem Configuration
    1.1.1 Disable unused filesystems
  1.2 Configure Software Updates
  1.3 Configure sudo
  1.4 Filesystem Integrity Checking
  1.5 Secure Boot Settings
  1.6 Additional Process Hardening
  1.7 Mandatory Access Control
    1.7.1 Configure SELinux
  1.8 Warning Banners
    1.8.1 Command Line Warning Banners
2 Services
  2.1 inetd Services
  2.2 Special Purpose Services
    2.2.1 Time Synchronization
  2.3 Service Clients
3 Network Configuration
  3.1 Network Parameters (Host Only)
  3.2 Network Parameters (Host and Router)
  3.3 Uncommon Network Protocols
  3.4 Firewall Configuration
    3.4.1 Ensure Firewall software is installed
    3.4.2 Configure firewalld
    3.4.3 Configure nftables
    3.4.4 Configure iptables
      3.4.4.1 Configure IPv4 iptables
      3.4.4.2 Configure IPv6 ip6tables
4 Logging and Auditing
  4.1 Configure System Accounting (auditd)
    4.1.1 Ensure auditing is enabled
    4.1.2 Configure Data Retention
  4.2 Configure Logging
    4.2.1 Configure rsyslog
    4.2.2 Configure journald
5 Access, Authentication and Authorization
  5.1 Configure cron
  5.2 SSH Server Configuration
  5.3 Configure authselect
  5.4 Configure PAM
  5.5 User Accounts and Environment
    5.5.1 Set Shadow Password Suite Parameters
6 System Maintenance
  6.1 System File Permissions
  6.2 User and Group Settings
```
