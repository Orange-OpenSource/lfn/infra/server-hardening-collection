---

- name: Get ipv6 status
  ansible.builtin.command:
    /bin/cat /sys/module/ipv6/parameters/disable
  check_mode: false
  register: ipv6_status
  changed_when: true
  tags:
    - always

- name: 3.1.1 Ensure IP forwarding is disabled
  ansible.builtin.shell: >
    set -eo pipefail
    /bin/grep -Els "^\s*net\{{ item }}\s*=\s*1" /etc/sysctl.conf
    /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf
    | while read filename;
    do
    /bin/sed -ri
    "s/^\s*(net\{{ item }}\s*)(=)(\s*\S+\b).*$/# *REMOVED* \1/" $filename;
    done;
    sysctl -w net{{ item | replace('\', '') }};
    sysctl -w net{{ item.split('\')[0] }}.route.flush=1
  loop:
    - '.ipv4\.ip_forward'
    - '.ipv6\.conf\.all\.forwarding'
  when: os_hardening_ip_forwarding_disabled
  tags:
    - 3.1.1

- name: 3.1.2 Ensure packet redirect sending is disabled
  become: true
  ansible.posix.sysctl:
    name: net.ipv4.conf.{{ item }}
    value: '0'
    state: present
  loop:
    - default.send_redirects
    - all.send_redirects
  tags:
    - 3.1.2

- name: 3.2.1 Ensure source routed packets are not accepted
  become: true
  block:
    - name: 3.2.1 Ensure source routed packets are not accepted (ipv6)
      ansible.posix.sysctl:
        name: "{{ item }}"
        value: 0
        state: present
        reload: false
      loop:
        - net.ipv6.conf.all.accept_source_route
        - net.ipv6.conf.default.accept_source_route
      when: ipv6_status.stdout == "0"
    - name: 3.2.1 Ensure source routed packets are not accepted (ipv4)
      ansible.posix.sysctl:
        name: "{{ item }}"
        value: "0"
        state: present
        reload: false
      loop:
        - net.ipv4.conf.all.accept_source_route
        - net.ipv4.conf.default.accept_source_route
  tags:
    - 3.2.1

- name: 3.2.2 Ensure ICMP redirects are not accepted
  become: true
  block:
    - name: 3.2.2 Ensure ICMP redirects are not accepted (ipv6)
      ansible.posix.sysctl:
        name: "{{ item }}"
        value: 0
        state: present
        reload: false
      loop:
        - net.ipv6.conf.all.accept_redirects
        - net.ipv6.conf.default.accept_redirects
      when: ipv6_status.stdout == "0"
    - name: 3.2.2 Ensure ICMP redirects are not accepted (ipv4)
      ansible.posix.sysctl:
        name: "{{ item }}"
        value: 0
        state: present
        reload: false
      loop:
        - net.ipv4.conf.all.accept_redirects
        - net.ipv4.conf.default.accept_redirects
  tags:
    - 3.2.2

- name: 3.2.3 Ensure secure ICMP redirects are not accepted
  become: true
  ansible.posix.sysctl:
    name: "{{ item }}"
    value: 0
    state: present
    reload: false
  loop:
    - net.ipv4.conf.all.secure_redirects
    - net.ipv4.conf.default.secure_redirects
  tags:
    - 3.2.3

- name: "3.2.[4,5,6,7] Packets logged, flows ignored, reverse Path Filter on"
  become: true
  ansible.posix.sysctl:
    name: "{{ item }}"
    value: "1"
    state: present
    reload: false
  loop:
    - net.ipv4.conf.all.log_martians
    - net.ipv4.conf.default.log_martians
    - net.ipv4.icmp_echo_ignore_broadcasts
    - net.ipv4.icmp_ignore_bogus_error_responses
    - net.ipv4.conf.all.rp_filter
    - net.ipv4.conf.default.rp_filter
  tags:
    - 3.2.4
    - 3.2.5
    - 3.2.6
    - 3.2.7

- name: 3.2.8 Ensure TCP SYN Cookies is enabled
  become: true
  ansible.posix.sysctl:
    name: net.ipv4.tcp_syncookies
    value: 1
    state: present
    reload: false
  notify: Set ipv4 parameters
  tags:
    - 3.2.8

- name: 3.2.9 Ensure IPv6 router advertisements are not accepted
  become: true
  ansible.posix.sysctl:
    name: "{{ item }}"
    value: 0
    state: present
    reload: false
  loop:
    - net.ipv6.conf.all.accept_ra
    - net.ipv6.conf.default.accept_ra
  when: ipv6_status.stdout == "0"
  notify: Set ipv6 parameters
  tags:
    - 3.2.9

- name: 3.3 Uncommon Network Protocols
  become: true
  ansible.builtin.blockinfile:
    path: /etc/modprobe.d/{{ item }}.conf
    create: true
    mode: 0644
    block:
      install {{ item }} /bin/true
  loop: "{{ os_hardening_uncommon_network_protocols }}"
  tags:
    - 3.3.1
    - 3.3.2
    - 3.3.3
    - 3.3.4

- name: 3.4.2.1 Ensure firewalld service is enabled and running
  become: true
  block:
    - name: 3.4.2.1 Ensure firewalld is installed
      ansible.builtin.package:
        name: firewalld
        state: present
    - name: 3.4.2.1 Ensure firewalld is running
      ansible.builtin.systemd:
        name: firewalld
        state: started
        enabled: true
        masked: false
  when: os_hardening_firewall == 'firewalld'
  tags:
    - 3.4.2.1

- name: 3.4.2.2 Ensure iptables is not enabled
  become: true
  ansible.builtin.package:
    name: iptables
    state: absent
  when: os_hardening_firewall != 'iptables'
  tags:
    - 3.4.2.2

- name: 3.4.2.3 Ensure nftables is not enabled
  become: true
  ansible.builtin.package:
    name: nftables
    state: absent
  when: os_hardening_firewall != 'nftables'
  tags:
    - 3.4.2.3

- name: 3.4.3.7 Ensure nftables service is enabled
  become: true
  block:
    - name: 3.4.3.7 Ensure nftables service is enabled (nftables installed)
      ansible.builtin.package:
        name: nftables
        state: present
    - name: 3.4.3.7 Ensure nftables service is enabled (nftables enabled)
      ansible.builtin.systemd:
        name: nftables
        enabled: true
  when: os_hardening_firewall == 'nftables'
  tags:
    - 3.4.3.7

- name: 3.4.3.8 Ensure nftables rules are permanent
  ansible.builtin.lineinfile:
    path: /etc/sysconfig/nftables.conf
    state: present
    line: 'include "/etc/nftables/nftables.rules"'
  when: os_hardening_firewall == 'nftables'
  tags:
    - 3.4.3.8

- name: Ensure iptables packages are installed
  become: true
  ansible.builtin.package:
    name:
      - iptables
      - iptables-services
    state: present
  when: os_hardening_firewall == 'iptables'

- name: Ensure ip6tables packages are installed
  become: true
  ansible.builtin.package:
    name: ip6tables
    state: present
  when: os_hardening_firewall == 'iptables' and ipv6_status.stdout == 0
