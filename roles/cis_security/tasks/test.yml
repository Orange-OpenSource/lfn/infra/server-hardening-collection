---
- name: check if the BENCHMARK_PASSWD environment variable is set
  ansible.builtin.fail:
    msg: "BENCHMARK_PASSWD variable is required"
  when: >
    os_hardening_security_benchmark_password is not defined or
    os_hardening_security_benchmark_password|length == 0

- name: check if the distribution has a cis cat profile
  ansible.builtin.fail:
    msg: "this distribution ({{ ansible_distribution }}) version\
          ({{ ansible_distribution_major_version }}) is not recognized"
  when: >
    os_hardening_security_benchmark_profile is not defined or
    os_hardening_security_benchmark_profile|length == 0

- name: install requirements
  ansible.builtin.import_role:
    name: orange.server_hardening.external_requirements
    tasks_from: test

- name: prepare and launch test
  block:
    - name: create destination folder
      ansible.builtin.file:
        path: "{{ os_hardening_security_benchmark_root_folder }}"
        state: directory
        recurse: true
        mode: 0700

    - name: copy ciphered test archive on the remote
      ansible.builtin.copy:
        src: "{{ os_hardening_security_benchmark_file_ciphered }}"
        dest: "{{ os_hardening_security_benchmark_file_ciphered_location }}"
        mode: '0600'

    - name: decipher the file
      ansible.builtin.shell: >
        echo '{{ os_hardening_security_benchmark_password }}' | \
        gpg --decrypt --output {{ os_hardening_security_benchmark_file }} \
        --pinentry-mode loopback --passphrase-fd 0 \
        {{ os_hardening_security_benchmark_file_ciphered }}
      args:
        chdir: "{{ os_hardening_security_benchmark_root_folder }}"

    - name: unarchive the files
      ansible.builtin.unarchive:
        src: "{{ os_hardening_security_benchmark_file_location }}"
        dest: "{{ os_hardening_security_benchmark_root_folder }}"
        remote_src: true

    - name: launch the benchmark test
      become: true
      ansible.builtin.command: >
        java -jar Assessor-CLI.jar \
        -b benchmarks/{{ os_hardening_security_benchmark_profile }} \
        -p "Level 2 - Server" -json -html
      args:
        chdir: "{{ os_hardening_security_benchmark_folder }}"
      register: benchmark_run

    - name: retrieve reports file
      become: true
      ansible.posix.synchronize:
        src: "{{ os_hardening_security_benchmark_report_folder }}"
        dest: "{{ base_dir }}"
        recursive: true
        use_ssh_args: true
        mode: pull

    - name: retrieve score
      ansible.builtin.set_fact:
        os_hardening_score:
          "{{ benchmark_run.stdout |
              regex_search('Total: (?P<score>[0-9.]+)%', '\\g<score>') |
              first | trim }}"

    - name: "fail if score under \
            {{ os_hardening_security_benchmark_min_value }}% \
            ({{ os_hardening_score }}%)"
      ansible.builtin.fail:
        msg: |
          Hardening score is {{ os_hardening_score }}%, which is lower than
          {{ os_hardening_security_benchmark_min_value }}%
      when:
        os_hardening_score|int < os_hardening_security_benchmark_min_value|int

  always:
    - name: remove benchmark folder
      ansible.builtin.file:
        path: "{{ os_hardening_security_benchmark_root_folder }}"
        state: absent
