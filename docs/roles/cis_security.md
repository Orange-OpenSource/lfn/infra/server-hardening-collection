# CIS security

## Purpose

Security hardening on main rules from CIS Benchmarks for CentOS 8

## Parameters

List of parameters for which there may be a sense to change the default value

<!-- markdownlint-disable line-length -->
| Variable                                | Purpose                                          | Default value                          |
|-----------------------------------------|--------------------------------------------------|----------------------------------------|
| `os_hardening_ntp_daemon`               | ntp daemon to use (chrony or ntp)                | chrony                                 |
| `os_hardening_chrony_servers`           | list of ntp servers to use                       | ntp_servers                            |
| `os_hardening_ip_forwarding_disabled`   | disable ip forwarding or not                     | false                                  |
| `os_hardening_firewall`                 | firewall used (nftables, firewalld or iptables)  | iptables                               |
| `os_hardening_sshd_allow_users`         | SSH daemon AllowUsers directive                  | centos                                 |
| `os_hardening_security_boot_password`   | password set on the bootloader                   | taken from BOOT_PASSWD env variable    |
| `os_hardening_authselect_profile`       | name of the authselect custom profile            | oronap-profile                         |
| `os_hardening_pam_password_minlen`      | minimum acceptable size for password             | 6                                      |
| `os_hardening_pam_password_minclass`    | min of required classes of chars for password    | 1                                      |
| `os_hardening_pam_auth_retry`           | prompt user at most N times before sending error | 3                                      |
| `os_hardening_pam_auth_remember`        | the last N passwords for each user are saved     | 5                                      |
| `os_hardening_password_mindays`         | mindays option of chage command                  | 0 (password changes at anytime)        |
| `os_hardening_password_inactive`        | inactive option of chage command                 | 7                                      |
| `os_hardening_password_maxdays`         | maxdays option of chage command                  | 99998                                  |
| `os_hardening_password_warndays`        | warndays option of chage command                 | 6                                      |
| `os_hardening_default_shell_tmout`      | default shell timeout in seconds                 | 900                                    |
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.server_hardening.cis_security

- ansible.builtin.import_role:
    name: orange.server_hardening.cis_security
  vars:
    os_hardening_password_maxdays: 30
```
