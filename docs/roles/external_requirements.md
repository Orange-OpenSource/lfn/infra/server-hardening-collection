# Install needed external requirements

## Purpose

Install external requirements needed by the other roles of this collection

## Parameters

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                            | Purpose                                  | Default value                                                                             |
|-------------------------------------|------------------------------------------|-------------------------------------------------------------------------------------------|
| `pip.repository`                    | which pip repository to use              | Will look at `<br>os_infra.pip.repository<br>`.<br>Default to `https://pypi.org/simple/`. |
<!-- markdownlint-enable no-inline-html line-length -->
## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.server_hardening.external_requirements

- ansible.builtin.import_role:
    name: orange.server_hardening.external_requirements
  vars:
    pip:
      index_url: http://myproxy/simple
```
