# CIS security

## Purpose

Security hardening from CIS Benchmarks for Ubuntu 20.04 (lockdown role)

## Parameters

For Ubuntu 20.04, you can have a file in your inventory if you want to override
variables from Ansible lockdown role.
Full list of variables can be found below
<https://github.com/ansible-lockdown/UBUNTU20-CIS/blob/1.1.0/defaults/main.yml>

Below, a list of parameters for which there may be interesting to change the
default value

```yaml
# IPv4 requirement toggle
ubtu20cis_ipv4_required: true

# IPv6 requirement toggle
ubtu20cis_ipv6_required: false

# Control 1.3.2
# These are the crontab settings for file system integrity enforcement
ubtu20cis_aide_cron:
  cron_user: root
  cron_file: /etc/crontab
  aide_job: '/usr/bin/aide.wrapper --config /etc/aide/aide.conf --check'
  aide_minute: 0
  aide_hour: 5
  aide_day: '*'
  aide_month: '*'
  aide_weekday: '*'

# Control 1.4.4
# THIS VARAIBLE SHOULD BE CHANGED AND INCORPORATED INTO VAULT
# THIS VALUE IS WHAT THE ROOT PW WILL BECOME!!!!!!!!
# HAVING THAT PW EXPOSED IN RAW TEXT IS NOT SECURE!!!!
ubtu20cis_root_pw: "Password1"

# Control 1.8.2
# This will be the motd banner must not contain the below items in order to be
# compliant with Ubuntu 20 CIS
# \m, \r, \s, \v or references to the OS platform
ubtu20cis_warning_banner: |
        Authorized uses only. All activity may be monitored and reported.

# Section 2 Control Variables
# Control 2.1.1.1
# ubtu20cis_time_sync_tool is the tool in which to synchronize time
# The two options are chrony, ntp, or systemd-timesyncd
ubtu20cis_time_sync_tool: "ntp"

# Control 2.1.1.2
# ubtu20cis_ntp_server_list is the list ntp servers
# ubtu20cis_ntp_fallback_server_list is the list of fallback NTP servers
ubtu20cis_ntp_server_list: "0.debian.pool.ntp.org 1.debian.pool.ntp.org"
ubtu20cis_ntp_fallback_server_list: "2.debian.pool.ntp.org 3.debian.pool.ntp.org"

# Control 2.1.1.3/2.1.1.4
# ubtu20cis_chrony_server_options is the server options for chrony
ubtu20cis_chrony_server_options: "minpoll 8"
# ubtu20cis_time_synchronization_servers are the synchronization servers
ubtu20cis_time_synchronization_servers:
  - 172.20.4.108
  - 172.20.1.104
# ubtu20cis_chrony_user is the user that chrony will use, default is _chrony
ubtu20cis_chrony_user: "_chrony"
# ubtu20cis_ntp_server_options is the server options for ntp
ubtu20cis_ntp_server_options: "iburst"

# ubtu20cis_firewall_package is the toggle for which firewall system is in use
# The valid options to use are ufw, nftables, or iptables
# Warning!! nftables is not supported in this role and will only message out if
# nftables is selected
# If using nftables please manually adjust firewall settings
ubtu20cis_firewall_package: "ufw"

# Control 3.5.1.5
# ubtu20cis_ufw_allow_out_ports are the ports for the firewall to allow
# if you want to allow out on all ports set variable to "all", example
# ubtu20cis_ufw_allow_out_ports: "all"
ubtu20cis_ufw_allow_out_ports:
  - 53 # DNS
  - 80 # HTTP
  - 123 # for ntp
  - 443 # HTTPS
  - 8080 # for proxy

# Control 4.2.1.5
# ubtu20cis_remote_log_server is the remote logging server
ubtu20cis_remote_log_server: 192.168.2.100

# Control 4.3
# ubtu20cis_logrotate_create_settings are the settings for the create parameter
# in /etc/logrotate.conf
# The permissions need to be 640 or more restrictive.
ubtu20cis_logrotate_create_settings: "0640 syslog adm"

# ubtu20cis_pass will be password based variables
# Control 5.5.1.1
# pass_min_days is the min number of days allowed between changing passwords.
# Set to 1 or more to conform to CIS standards
# Control 5.5.1.2
# max_days forces passwords to expire in configured number of days.
# Set to 365 or less to conform to CIS standards
# Control 5.5.1.3
# warn_age is how many days before pw expiry the user will be warned.
# Set to 7 or more to conform to CIS standards
# Control 5.5.1.4
# inactive the number of days of inactivity before the account will lock.
# Set to 30 day sor less to conform to CIS standards
ubtu20cis_pass:
    max_days: 365
    min_days: 1
    warn_age: 7
    inactive: 30

# Control 5.5.5
# Session timeout setting file (TMOUT setting can be set in multiple files)
# Timeout value is in seconds. Set value to 900 seconds or less
ubtu20cis_shell_session_timeout:
    file: /etc/profile.d/tmout.sh
    timeout: 900
```

## Examples

Please note that the CIS (Center for Internet Security) rules are approved
by a legal authority and reviewed by security teams.

The list of rules can be found in the official
[lockdown documentation][1].
But this list does not provide a description of the rules, the description of
each rule is usually complex and may be found on the
[CIS web site](https://www.cisecurity.org).

It is possible through the configuration file to exclude some rules by setting
the rule lockdown identifier to false but it must be validated by the Orange
security team first.

```yaml
- ansible.builtin.import_role:
    name: orange.server_hardening.lockdown

- ansible.builtin.import_role:
    name: orange.server_hardening.lockdown
  vars:
    ubtu20cis_rule_1_1_11: false
```

[1]: https://github.com/ansible-lockdown/UBUNTU20-CIS/blob/1.1.0/defaults/main.yml
