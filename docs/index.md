# OS hardening collection

The goal of this collection consists in hardening any server that
could be used for an ORONAP deployment.
This collection supports CentOS 8 and Ubuntu 20.04 servers.
It is based on Center for Internet Security recommendations.
[CIS benchmarks](https://www.cisecurity.org/cis-benchmarks/)

It consists in two playbooks (run.yml and test.yml) and two roles
respectively for CentOS 8 (cis_security) and Ubuntu 20.04 (lockdown).

Notice that the lockdown role is in fact a reference to an external
role from the Internet through the mechanism of git submodule.
[lockdown](https://github.com/ansible-lockdown/UBUNTU20-CIS)

The run.yml playbook leverages the associated role according to the
underlying Operating System used (CentOS 8 or Ubuntu 20.04).

The test.yml playbook evaluates the hardening quality of a CentOS 8
or Ubuntu 20.04 by launching a tool from Center for Internet Security.

## Playbooks
<!-- markdownlint-disable line-length -->
| Playbook                      | Description                                                  |
| ----------------------------- | ------------------------------------------------------------ |
| [run](./playbooks/run.md)     | Playbook to harden ORONAP servers                            |
| [test](./playbooks/test.md)   | Playbook to evaluate the hardening quality of ORONAP servers |
<!-- markdownlint-enable line-length -->

## Roles
<!-- markdownlint-disable line-length -->
| Role                                                      | Description                                                                         |
| --------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| [cis_security](./roles/cis_security.md)                   | CentOS 8 server hardening based on Center for Internet Security recommendations     |
| [lockdown](./roles/lockdown.md)                           | Ubuntu 20.04 server hardening based on Center for Internet Security recommendations |
| [external requirements](./roles/external_requirements.md) | Management of external requirements                                                 |
<!-- markdownlint-enable line-length -->

## Versions

| ORONAP version | Collection sha1                          |
| -------------- | ---------------------------------------- |
| Honfleur       | 565c79bccba796759ef1c5b3d061f1a181e01888 |
| Istres         | 565c79bccba796759ef1c5b3d061f1a181e01888 |
