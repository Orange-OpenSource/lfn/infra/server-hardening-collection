# Run playbook

## Purpose

Install external requirements and run the CentOS 8 or Ubuntu 20.04 hardening

## Inventory

The inventory must provide a group `hardening` with **one** server inside.

This playbook mandates Ability to connect to the targeted Kubernetes via a kube
config file associated in default location
(`{{ ansible_user_dir }}/.kube/config`) for the chosen user on the server.

## Mandatory parameters

For CentOS 8, a BOOT_PASSWD environment variable must exist
It is used to set the bootloader password (rule 1.5.2)

## Tags

The playbook proposes the following tags:

| Tag             | Purpose                               |
|-----------------|---------------------------------------|
| `hardening`     | for controlling OS hardening          |

## Examples

Inventory:

```ini
[hardening]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/run.yml
```
