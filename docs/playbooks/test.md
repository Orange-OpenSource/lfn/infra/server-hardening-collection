# Test playbook

## Purpose

Evaluate the hardening quality of a CentOS 8 or Ubuntu 20.04 system

## Inventory

The inventory must provide a group `hardening` with **one** server inside.

## Tags

The playbook proposes the following tags:

| Tag             | Purpose                                 |
|-----------------|-----------------------------------------|
| `hardening`     | for controlling OS hardening evaluation |

## Examples

Inventory:

```ini
[hardening]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/test.yml
```
